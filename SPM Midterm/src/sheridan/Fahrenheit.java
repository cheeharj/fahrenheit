package sheridan;

public class Fahrenheit 
{

	public static void main(String[] args) {
		System.out.println(convertFromCelsius(0));
	}
	
	public static int convertFromCelsius(int input)
	{		
		return ((9/5) * input + 32);
	}
	
}
