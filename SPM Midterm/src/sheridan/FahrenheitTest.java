package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testConvertFromCelsius()
	{		
		// Regular path. This should work by default.
		assertTrue("Regular path failed.", true);
		
		// Exceptional path.
		assertNotNull("Exceptional path failed.", Fahrenheit.convertFromCelsius(0));
		
		// Boundry test
		assertEquals("Boundry test for output failed.",
				Fahrenheit.convertFromCelsius(0), (int)Math.ceil(Fahrenheit.convertFromCelsius(0)));
		
	}

}
